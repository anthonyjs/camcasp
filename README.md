#             CamCASP

##   An electronic structure program for studying intermolecular interactions
 
Alston J. Misquitta
and
Anthony J. Stone

with many important modules and contributions from
Robert Bukowski, Wojciech Cencek and others.

CamCASP (Cambridge package for Calculation of Anisotropic Site
Properties) is a collection of scripts and programs written by
Alston Misquitta and Anthony Stone for the calculation ab initio of
distributed multipoles, polarizabilities, dispersion coefficients and
repulsion parameters for individual molecules, and interaction
energies between pairs of molecules using SAPT(DFT). The program is
still being actively developed. In addition to the programs included
in the package, CamCASP uses some other programs: Orient, and an ab
initio program, normally Dalton or NWChem or Psi4. See the user's
guide, included in the package or available separately from http://www-stone.ch.cam.ac.uk/documentation/camcasp/users_guide.pdf, for full details. 

Version 6.1, with many enhancements and improvements, is now available
from gitlab.com. To clone the package into a new empty directory, e.g.
camcasp6.1, use the command  
```
   git clone https://gitlab.com/anthonyjs/camcasp.git camcasp6.1
```
If you require the version for Mac OS, the command is slightly
different:  
```
   git clone https://gitlab.com/anthonyjs/camcasp.git --branch CamCASP-6.1-macos camcasp6.1
```

See the
[INSTALL.md](https://gitlab.com/anthonyjs/camcasp/-/wikis/INSTALL.md)
file for installation instructions.
See the User's Guide in the docs directory for detailed instructions
on using the program.

Some directory information:

 directory | contains
 --- | ---
basis/    | Basis set libraries  
docs/     | Documentation  
bin/      | Scripts and binaries  
examples/ | Examples  
data/     | Data needed by some of the programs.  
methods/  | CamCASP data for various procedures, and template cluster files.  
tests/    | Test scripts. See tests/README for details.  

Version information is in: VERSION

Record of changes is in:   ChangeLog
