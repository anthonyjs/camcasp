#!/usr/bin/python
#  -*-  coding:  iso-8859-1  -*-

"""Script to strip out AIM (ISA) code from CamCASP.
"""

from camcasp import die
import argparse
import glob
import os
import re
import shutil
import subprocess

home = os.environ["HOME"]
trunk = home + "/camcasp/trunk"
distrib = home + "/camcasp/distrib"


parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Strip out AIM (ISA) code from CamCASP.
""", epilog="""
No need to execute command from any particular directory.
The full files are taken from the directory specified by --trunk,
and the stripped files are in the directory specified by --distrib.
Default is to take them from $HOME/camcasp/{trunk,distrib}.
""")

parser.add_argument("--trunk", help="Path to the CamCASP trunk directory",
                    default=home + "/camcasp-trunk")
parser.add_argument("--distrib", help="Path to the CamCASP distribution directory",
                    default=home + "/camcasp-distrib")


args = parser.parse_args()

#  Remove ISA source files
for file in ["src/stockholder.F90", "src/display.F90",
             "data/camcasp-cmnds/isa-display"]:
  if os.path.exists(os.path.join(distrib,file)):
    os.remove(os.path.join(distrib,file))

#  skip is the number of lines to be skipped, not counting the one
#  which causes it to be set.

#  Remove ISA definitions from types.F90
skip = 0
with open(trunk + "/src/types.F90") as TYPX:
  with open(distrib + "/src/types.F90","w") as TYP:
    for line in TYPX:
      m = re.match(r'type.* :: (\w+)$', line)
      if m:
        type = m.group(1)
      if skip > 0:
        skip -= 1
      elif re.search(r'ISAcharge', line, flags=re.I) and type == "atom":
        pass
      elif re.search(r'Stockholder', line) and type == "atom":
        skip = 14
      elif re.search(r'Stockholder', line) and type == "molecule":
        skip = 16
      elif re.search(r'A%NumStockAtoms', line):
        skip = 2
      else:
        TYP.write(line)
      
#  Remove ISA references from main_parser.F90
skip = 0
with open(trunk + "/src/main_parser.f90") as MPX:
  with open(distrib + "/src/main_parser.f90","w") as MP:
    for line in MPX:
      if skip > 0:
        skip -= 1
      elif re.match(r'use StockholderModule', line, flags=re.I):
        skip = 1
      elif re.search(r'STOCKHOLDER', line):
        skip = 3
      else:
        MP.write(line)


#  Remove ISA references from type_operations.F90
skip = 0
with open(trunk + "/src/type_operations.F90") as TYPX:
  with open(distrib + "/src/type_operations.F90","w") as TYP:
    for line in TYPX:
      if skip > 0:
        skip -= 1
      elif re.search(r'Stockholder', line):
        skip = 3
      elif re.search(r'ISA charge', line):
        skip = 3
      else:
        TYP.write(line)

#  Remove ISA references from molecule_operations.F90
skipping = False
skip = 0
with open(trunk + "/src/molecule_operations.F90") as MOLX:
  with open(distrib + "/src/molecule_operations.F90","w") as MOL:
    for line in MOLX:
      if skip > 0:
        skip -= 1
      elif skipping:
        if re.search(stop_skip, line):
          skipping = False
      elif re.search(r'subroutine initialize_stockholder_atoms', line):
        skipping = True
        stop_skip = "subroutine initialize_stockholder_atoms"
      elif re.search(r'subroutine find_site_neighbours', line):
        skipping = True
        stop_skip = "subroutine find_site_neighbours"
      elif re.search(r'\%AtomsInitialized', line):
        skip = 7
      elif re.search(r'Initialized stockholder atoms', line):
        pass
      elif re.search(r'stockholder', line):
        skip = 6
      elif re.search(r'all Stockholder sites', line):
        skip = 5
      else:
        MOL.write(line)

#  Remove ISA references from function_expansion_operations.F90
skipping = False
skip = 0
with open(trunk + "/src/function_expansion_operations.F90") as FNX:
  with open(distrib + "/src/function_expansion_operations.F90","w") as FN:
    for line in FNX:
      if skip > 0:
        skip -= 1
      elif skipping:
        if re.search(stop_skip, line):
          skipping = False
      elif re.match(r'public \:\: AtomRho_D2FuncExpansion', line):
        skip = 10
      elif re.search(r'subroutine AtomRho_D2FuncExpansion', line):
        skipping = True
        stop_skip = "subroutine AtomRho_D2FuncExpansion"
      elif re.search(r'subroutine MolRho2AtomRho_FuncExpansion', line):
        skipping = True
        stop_skip = "subroutine MolRho2AtomRho_FuncExpansion"
      else:
        FN.write(line)

#  Remove ISA references from num_integrals.F90
skipping = False
skip = 0
with open(trunk + "/src/num_integrals.F90") as NUMX:
  with open(distrib + "/src/num_integrals.F90","w") as NUM:
    for line in NUMX:
      if skip > 0:
        skip -= 1
      elif skipping:
        if re.search(stop_skip, line):
          skipping = False
      elif re.match(r'public \:\: Chi_rho_w_overlap', line):
        pass
      elif re.search(r'subroutine Chi_rho_w_overlap', line):
        skipping = True
        stop_skip = "subroutine Chi_rho_w_overlap"
      else:
        NUM.write(line)

#  Remove ISA references from df_monomer.F90
skipping = False
skip = 0
with open(trunk + "/src/df_monomer.F90") as DFX:
  with open(distrib + "/src/df_monomer.F90","w") as DF:
    for line in DFX:
      if skip > 0:
        skip -= 1
      elif skipping:
        if re.search(stop_skip, line):
          skipping = False
      elif re.search(r'subroutine do_DFrhoW_monomer', line):
        skipping = True
        stop_skip = "subroutine do_DFrhoW_monomer"
      elif re.search(r'case\(5\)', line):
        skip = 15
      else:
        DF.write(line)

#  Remove ISA references from e1_exch.F90
skipping = False
skip = 0
with open(trunk + "/src/e1exch.F90") as EXX:
  with open(distrib + "/src/e1exch.F90","w") as EX:
    for line in EXX:
      if skip > 0:
        skip -= 1
      elif skipping:
        if re.search(stop_skip, line):
          skipping = False
      elif re.search(r'call distributed_density_overlap_stockholder', line):
        skip = 3
      elif re.search(r'subroutine distributed_density_overlap_stockholder', line):
        skipping = True
        stop_skip = "subroutine distributed_density_overlap_stockholder"
      else:
        EX.write(line)


#  Remove ISA references from dist_moments.F90
skipping = False
skip = 0
with open(trunk + "/src/dist_moments.F90") as DMX:
  with open(distrib + "/src/dist_moments.F90","w") as DM:
    for line in DMX:
      if skip > 0:
        skip -= 1
      elif skipping:
        if re.search(stop_skip, line):
          skipping = False
      elif re.search(r'case\(\'ISA\'\)', line):
        skip = 23
      else:
        DM.write(line)

#  Remove ISA references from IO_parser.F90
skipping = False
skip = 0
with open(trunk + "/src/IO_parser.F90") as IOX:
  with open(distrib + "/src/IO_parser.F90","w") as IO:
    for line in IOX:
      if skip > 0:
        skip -= 1
      elif skipping:
        if re.search(stop_skip, line):
          skipping = False
      elif re.search(r'case\(\'ATOMS\'\)', line):
        skip = 15
      else:
        IO.write(line)


#  Remove references to ISA routines from Dependencies
skip = 0
with open(trunk + "/misc/Dependencies") as DEPX:
  with open(distrib + "/misc/Dependencies","w") as DEP:
    for line in DEPX:
      if skip > 0:
        skip -= 1
      elif re.match(r'(StockholderModule|display).mod', line, flags=re.I):
        pass
      elif re.search(r'stockholdermodule.mod', line):
        line = re.sub(r'stockholdermodule.mod', "", line)
        DEP.write(line)
      elif re.search(r'display.mod', line):
        line = re.sub(r'display.mod', "", line)
        DEP.write(line)
      elif re.match(r'display.o', line):
        skip = 3
      elif re.match(r'stockholder.o', line):
        skip = 8
      else:
        DEP.write(line)

#  Remove references to ISA routines from SRCS.list
skip = 0
with open(trunk + "/misc/SRCS.list") as SRCX:
  with open(distrib + "/misc/SRCS.list","w") as SRC:
    for line in SRCX:
      if skip > 0:
        skip -= 1
      elif re.search(r'stockholder.F90', line):
        line = re.sub(r'stockholder.F90', "", line)
        SRC.write(line)
        skip = 1
      else:
        SRC.write(line)

#  Remove references to ISA routines from SRCS.list
skip = 0
with open(trunk + "/distrib/source_files") as SRCX:
  with open(distrib + "/distrib/source_files","w") as SRC:
    for line in SRCX:
      if skip > 0:
        skip -= 1
      elif re.search(r'(stockholder.F90|display.F90|marching_cubes.F90)', line, flags=re.I):
        pass
      else:
        SRC.write(line)

