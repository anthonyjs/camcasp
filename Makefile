#=============================
#  User-defined variables:
#  Choices for ARCH: x86-64, osx, native or "."
#  It can be specified on the command line or by setting the environment
#  variable ARCH.
#  If "native" is specified, provide a symbolic link "native" that points
#  to the appropriate directory; e.g. cd to the base directory and issue
#  the command
#    ln -s x86-64 native
#  If "." is specified, copy the contents of the appropriate ARCH directory
#  recursively to the base directory:
#    cp -r x86-64/* .
#  or if you're only going to use one particular compiler, copy that
#  subdirectory only:
#    cp -r x86-64/gfortran .
#  If there is no suitable ARCH/COMPILER subdirectory, construct one
#  following the pattern of existing ones.
#  In any case, inspect the ARCH/COMPILER/{exe,debug}/Flags files and make any
#  necessary changes to library specifications, compiler options, etc.
#
SHELL       := /bin/bash
BASE        := ${shell pwd}
# COMPILER  := gfortran
MACHINE     := ${shell hostname -s}
ifndef ARCH
  ARCH        := osx
endif
DEBUG       := 
INSTALL_DIR := $(HOME)/bin
#End of user-defined variables
#=============================

include VERSION
DATE=$(shell date '+%a %d %b %Y %H:%M:%S %Z')

# Get the version control id.  Works with either svn or git or if no VCS is
# used.
# # Outputs a string.
VCS_VER:='$(shell set -o pipefail && echo -n \" && ( ( (svn info || git svn info) | grep 'Revision' | sed -e 's/Revision: //' ) ||  git log --max-count=1 --pretty=format:%H || echo -n 'Not under version control.' ) 2> /dev/null | tr -d '\r\n'  && echo -n \")'
# Test to see if the working directory contains changes. Works with either
# svn or git.
# If the working directory contains changes (or is not under version control)
# then the _WORKING_DIR_CHANGES flag is set.
# WORKING_DIR_CHANGES := $(shell (if [[ -e .svn ]]; then svn st -q | xargs -i test -z {} ; else (git diff-index --quiet --cached HEAD --ignore- submodules -- && git diff-files --quiet --ignore-submodules) 2> /dev/null ; fi) || echo -n "-D_WORKING_DIR_CHANGES")
# CHANGES := '"$(shell (svn st -q src | sed -e 's/[M,A] / /;s,src/,,;s/[ \t]*/ /' ))"'
#
#COMPILER_VER:='$(shell echo -n \" && ( $(COMPILER) -v ) && echo -n \" )'
COMPILER_VER:='"unknown"'

NULL=''
#ifeq ($(strip $(MACHINE)),$(NULL))
ifndef MACHINE
  MACHINE := $(shell hostname -s)
  #${shell echo 'Variable MACHINE is not defined'}
  #${shell exit}
endif

ifndef COMPILER
  COMPILER := gfortran
endif
ifeq ($(COMPILER),nag)
  COMPILER := nagfor
endif

ifndef NAME
  NAME := camcasp
endif

ifdef PRIVATE
  PRIVDIR := "${BASE}/src/${PRIVATE}:"
endif

ifneq "$(DEBUG)" ""
  ifeq ($(NAME),pfit)
    DIR := ${ARCH}/$(COMPILER)/debug/pfit
  else
    DIR := ${ARCH}/$(COMPILER)/debug
  endif
else
  ifeq ($(NAME),pfit)
    DIR := ${ARCH}/$(COMPILER)/exe/pfit
  else
    DIR := ${ARCH}/$(COMPILER)/exe
  endif
endif

PASS    = BASE=${BASE} DIR=${DIR} FRC=${FRC} NAME=${NAME} \
   VERSION=${VERSION} PATCHLEVEL=${PATCHLEVEL} DEBUG="${DEBUG}" \
   ARCH=${ARCH} MACHINE=${MACHINE} INSTALL_DIR=${INSTALL_DIR} \
   VCS_VER=${VCS_VER} COMPILER_VER=${COMPILER_VER} \
   WORKING_DIR_CHANGES=${WORKING_DIR_CHANGES} PRIVDIR="${PRIVDIR}"

.SUFFIXES:

normal: force
	mkdir -p ${DIR}; cd ${DIR}; ${MAKE} -f ${BASE}/Makefile_body \
            COMPILER=${COMPILER} ${PASS} CHANGES=${CHANGES}
	ln -f ${BASE}/${DIR}/${NAME} ${ARCH}/$(COMPILER)
	ln -fs ../${ARCH}/$(COMPILER)/${NAME} ./bin

all: camcasp process cluster casimir pfit

testbuild: force
	cd ${DIR}; ${MAKE} -f ${BASE}/Makefile_body_test COMPILER=${COMPILER} \
            ${PASS}
	ln -fs ${BASE}/${DIR}/${NAME} ./bin

camcasp process cluster casimir pfit gdma res2disp:
	${MAKE} NAME=$@

interfaces: force
	cd interfaces/nwchem; ${MAKE} clean; ${MAKE}
	cd interfaces/gamessus; ${MAKE} clean; ${MAKE}
	cd interfaces/dalton; ${MAKE} clean; ${MAKE}

# debug: force
# 	cd ${COMPILER}/debug; \
#             ${MAKE} -f ${BASE}/Makefile_body COMPILER=${COMPILER} \
#             ${PASS}

depend depend_pfit: force
	cd ${DIR}; ${MAKE} -f ${BASE}/Makefile_body BASE=${BASE} DIR=${DIR} \
           NAME=${NAME} VERSION=${VERSION} DEBUG=${DEBUG} \
           INSTALL_DIR=${INSTALL_DIR} MACHINE=${MACHINE} \
           COMPILER=${COMPILER} ${PASS} $@

#  make patches OLD=<previous patch level> [BASE_FILES="file file ..."] \
#         {SOURCE_FILES="file file ..."]
#  BASE_FILES lists any files that are to be added to the base distribution.
#  File-names should be relative to the CamCASP base directory.
#  SOURCE_FILES likewise.
patches: base_patches source_patches

base_patches:
	rm -f patch/*.patch
	./bin/make_patches.pl ~ camcasp-${VERSION}.${OLD} camcasp-${VERSION} \
             -f base_files
	tar cvf base_patches-${VERSION}.${OLD}.tar patch/*.patch
ifneq (strip($(BASE_FILES)),)
	tar rvf base_patches-${VERSION}.${OLD}.tar $(BASE_FILES)
endif
	gzip base_patches-${VERSION}.${OLD}.tar
	mv base_patches-${VERSION}.${OLD}.tar.gz base_patches-${VERSION}.${OLD}.tgz
	rm -f patch/*.patch

source_patches:
	rm -f patch/*.patch
	./bin/make_patches.pl ~ camcasp-${VERSION}.${OLD} camcasp-${VERSION} \
             -f source_files
	tar cvf source_patches-${VERSION}.${OLD}.tar patch/*.patch
ifneq (strip($(SOURCE_FILES)),)
	tar rvf source_patches-${VERSION}.${OLD}.tar $(SOURCE_FILES)
endif
	gzip source_patches-${VERSION}.${OLD}.tar
	mv source_patches-${VERSION}.${OLD}.tar.gz source_patches-${VERSION}.${OLD}.tgz
	rm -f patch/*.patch
	rm -f patch/*.patch

#  Carry out tests. See tests/README for details.
#  ARGS is the argument list, enclosed in quotes.
test tests: force
	cd tests; ./run_tests.py ${ARGS}

# install:
#	cp -p bin/${NAME} ${INSTALL_DIR}

distrib: doc force
	cd distrib; ${MAKE} NAME=${NAME} VERSION=${VERSION}

doc: force
	cd doc; ${MAKE}

set_path: force
	cd src; ../bin/set_path.pl ${BASE}

force:

clean: 
	cd ${DIR}; rm -f core *.o *.mod *.stb *~
	cd ${DIR}/pfit; rm -f core *.o *.mod *.stb *~

get:
	cd ..; ${MAKE} get DIR=spherical_harmonics DRYRUN=

# DO NOT DELETE
