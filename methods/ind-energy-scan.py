#!/usr/bin/python
#  -*-  coding:  iso-8859-1  -*-

import argparse
import os.path

parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description='''Append CamCASP commands to <job>.cks file.
''',epilog='''
CamCASP method ind-energy-scan

Calculate the induction energy response of a molecule to a unit charge
placed on each point in turn of a grid, usually over a surface around
the molecule. The grid points are specified in a file that begins
  Translations
  [Skip-first]
  Points <n>
where <n> is the number of points. This header is followed by the
coordinates of the points, one to a line. The points may each be preceded
by an index integer; in this case the "Skip-first" header line is needed,
otherwise not.

The Orient program can be used to construct a suitable grid file.

The name of the points file should be given as an argument to the script.
If the file is not in the job directory, it should be specified by its
full path, or it can be copied into the new job directory using the
IMPORT command in the cluster (<job>.clt) file.

''')

parser.add_argument("job", help="Job name (file prefix)")
parser.add_argument("--grid", default="grid.pts",
           help="File containing the list of points (default grid.pts)")

args = parser.parse_args()

if not os.path.exists(grid):
  print "Can't find file {}".format(grid)
  exit(1)

with open(args.job+".cks","a") as CKS:
  CKS.write('''

Edit
  NEIGHBOURS TYPE = OVR EPS = 0.01 PRINT
End

SET QUAD
  Type Gauss-Legendre
  Beta 0.5
END

BEGIN GRID
  Molecule  A
  Angular 400
  Radial  100
END

SET PROPAGATOR
  Type CKS
  Hessians Internal
  DF with constraints
  DF-integrals
END

SET DF-INTEGRALS
  DF-TYPE-MONOMER NN
END
SET DF
  Solver LU (Options are LU and GELSS )
END

BEGIN DF
  Molecule  A
  Type     NN
  Eta    =    0.0 
  Lambda = 1000.0 
  Gamma  =    0.0
  Print only normalization constraints
END
BEGIN DF
  Molecule  A
  Type     NN
  Eta    = 0.0 
  Lambda = 0.0 
  Gamma  = 0.0
  Print only normalization constraints
END

SET NEW-PROP
  Kernel ALDA
  DF  ( For constrained DF use: C-DF )
  ! These parameters control the quality of the LR-DFT
  ! kernel. Additionally, the integration grid affects
  ! the kernel.
  KERNEL-INTEGRAL-PARAMETERS
    INFINITY-CONTROL-METHOD FD
    RHO-EPS  = 1e-8
    F-MAX    = 1000.0
    FD-DELTA = 0.01
    FD-ALPHA = 1.0
    KERNEL-INTEGRAL-CUTOFF =   0.10E-07
  END
  SOLVER LU ( Options: GELSS )
END
SET PROPAGATOR
  Type CKS
  Hessians Internal
  DF without constraints
  DF-integrals
END

SET DF-INTEGRALS
  DF-TYPE-MONOMER NN
  DF-TYPE-DIMER OV
END

SET DF
  Eta = 0.0
  Lambda = 0.0
  REDO-DF-ON-ROTATION  True
  Solver LU
END
BEGIN DF
  Molecule A
  Type NN
  Print only normalization constraints
END

SET QUAD
  Type Gauss-Legendre
  Beta 0.5
END

BEGIN energy-scan
  Scan molecule A with charge 1.0
  Energies E2ind
  Energy-file induction-scan
  Points
    !  Provide points file (created by Orient)
    !  Give full path or put the file in the job directory
    !  e.g.
    !  file /home/ajs1/molecules/piracetam/aug-cc-pVDZ_-63/piracetam_vdW1.50.pts
    !  or make a symbolic link grid.pts to the appropriate file
    file  {g}
  ---
End

Finish


  '''.format(g=grid))
