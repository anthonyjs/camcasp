#!/usr/bin/python
#  -*-  coding:  iso-8859-1  -*-

"""Convert CamCASP method file to Python script
"""

import argparse
import re
import os.path
# import string
# import subprocess

parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Convert CamCASP method file to Python script.
""",epilog="""
./convert.to.py <method>
""")


parser.add_argument("method", help="Method file")
# parser.add_argument("--job", help="CamCASP job name")

args = parser.parse_args()

method = args.method
if re.search(r'tmpl$', method):
  print "This is a template file"
  exit(1)
if not os.path.exists(method):
  print "Can't find file", method
  exit(2)

with open(method+".py","w") as MPY:
  MPY.write("""#!/usr/bin/python
#  -*-  coding  iso-8859-1  -*-

parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description='''Append CamCASP commands to <job>.cks file.
''',epilog='''
CamCASP method {}

'''.format(method)

with open(args.job+".cks","a") as CKS:
  CKS.write('''
""")
  with open(method) as M:
    for line in M:
      if re.match(r' *(End-)?CamCASP-(comm|cmnd)', line, flags=re.I):
        pass
      elif re.match(r'# +-\*- +coding', line, flags=re.I):
        pass
      else:
        MPY.write(line)
    MPY.write("""
''')
""")
