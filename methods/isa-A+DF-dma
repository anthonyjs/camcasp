#  -*- coding: utf-8 -*-
CamCASP-commands

Edit
  NEIGHBOURS TYPE = OVR   EPS = 0.01  PRINT
End

SET QUAD
  Type Gauss-Legendre
  Beta 0.5
END

BEGIN GRID
  Molecule A
  Angular 200
  Radial  40
END

SET DF-INTEGRALS
  DF-TYPE-MONOMER NN
END

BEGIN DF
  Type RHO
  Eta = 0.0
  Lambda = 1000.0
  Gamma = 0.0
  Print only normalization constraints
  Solver GELSS Condition = 1e-15
END

Begin ISA
  DF = Drho-C
  ! W-INIT = Drho-C
  W-INIT = ONE-GTO   ALPHA0 = 1.0
  ISA-Algorithm A+DF  Zeta = 0.1
  DF-PARAMETERS Lambda = 1000.0
  Solver LU
  !  Symmetry
  Convergence
    Convergence-Type W
    EPS-Norm = 1.0e-09
    EPS-Q    = 1.0e-4
    Max-Iter = 120
    W-Damping = 0.0
    W-Mix-Fraction = 0.0  Skip-Iterations = 20
    W-Eps = 0.17  S-Block-Only  Couple  Activate-at 1.0e-05
    Positive-W   Lambda = 0.001  Auto  Max-Alpha = 0.2
    Tail-Iterations = 30
  End
  W-TAILS
    Func = 1
    R1-Multiplier = 1.5
    R2-Multiplier = 2.5
    Fit-Type = 3
    W-Tests
  END
End

!  Calculate multipoles using ISA densities
Begin Multipoles
  DF Type ISA-GRID
  Rank 4
End

!  Generate isodensity surface grids for use in Orient
Begin Display
  Molecular-density
    Slater-multiplier 5.0
    isodensity 0.001
    step = 0.3 bohr
  END
  AIM-ATOMS
    ISOdensity   = 0.001
    AIM-METHOD   = ISA  Grid-based
    Density-Type = RHO-C
    ATOMS        = ALL    ( specify list of atom labels or specify ALL for all atoms )
    W-MIN        = 1e-6   ( outer limit of scan determined to be radius at )
                          ( which shape function is this value )
    R-MAX        = 5.0 Bohr  ( with a maximum radius of this )
    SLATER-MULTIPLIER = 10.0 ( can also specify max radius as a )
                             ( multiple of Slater radius )
    DR           = 0.2 Bohr  ( step size of grid )
  END
End

! Fine integration grid needed for DMA calculation

BEGIN GRID
  Molecule A
  Angular 590
  Radial  80
END

Begin GDMA
  Bohr
  !  Original DMA
  Multipoles
    Switch 0.0
    Limit  5
    Punch DMA_Z0_L5.mom
  Start
  !  Revised DMA
  Multipoles
    Switch 4.0
    Limit  5
    Punch DMA_Z4_L5.mom
  Start
End

End-CamCASP-commands
